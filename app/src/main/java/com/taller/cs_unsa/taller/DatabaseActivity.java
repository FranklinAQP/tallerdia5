package com.taller.cs_unsa.taller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DatabaseActivity extends AppCompatActivity {

    private DatabaseReference alumnosDB;
    private Button Agregar;
    private EditText Nombre;
    private TextView NombreActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        Agregar = (Button) findViewById(R.id.addalumno);
        Nombre = (EditText) findViewById(R.id.alumno);
        NombreActual = (TextView) findViewById(R.id.nombreactual);
        //Firebase
        alumnosDB = FirebaseDatabase.getInstance().getReference().child("alumno");

        Agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alumno = Nombre.getText().toString().trim();
                alumnosDB.setValue(alumno);
            }
        });

        // Read from the database
        alumnosDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                NombreActual.setText(value);
                Log.d("estado", "Valor editado: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("estado", "Error subiendo datos.", error.toException());
            }
        });
    }


}
