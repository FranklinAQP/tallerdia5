package com.taller.cs_unsa.taller;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private Button My_location;
    private Button New_location;
    private TextView mLatitudeText;
    private TextView mLongitudeText;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location My_position;
    private Marker Marker_My_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        My_location = (Button) findViewById(R.id.my_location);
        New_location = (Button) findViewById(R.id.new_location);
        mLatitudeText = (TextView) findViewById(R.id.last_latitud);
        mLongitudeText = (TextView) findViewById(R.id.last_longitud);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                My_position = location;
            }
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
            @Override
            public void onProviderEnabled(String provider) {
            }
            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.i("NavigationDrawer", "No conecta a GPS");
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.i("NavigationDrawer", "No tiene permisos de ACCESS_FINE_LOCATION");
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.i("NavigationDrawer", "No tiene permisos de ACCESS_COARSE_LOCATION");
                }
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET
                }, 10);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.i("NavigationDrawer", "SI tiene permisos de ACCESS_FINE_LOCATION");
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.i("NavigationDrawer", "SI tiene permisos de ACCESS_COARSE_LOCATION");
                }
                return;
            }
        } else {
            Log.i("NavigationDrawer", "Conexion a GPS Activa");
            Configuration_Buttons();
        }

        My_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("NavigationDrawer", "Click en My_location");
                locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
                if(My_position!=null){
                    Log.i("NavigationDrawer", "GPS funcionando");
                    mLatitudeText.setText(""+My_position.getLatitude());
                    mLongitudeText.setText(""+My_position.getLongitude());
                    mMap.clear();
                    GoToMyLocationZoom(My_position.getLatitude(), My_position.getLongitude(), 14);
                    //
                }else{
                    Log.i("NavigationDrawer", "Esperando datos de GPS");
                }
            }
        });

        New_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("NavigationDrawer", "Click en New_location");
                LatLng nuevapos = new LatLng(-16.3988900, -71.5350000);
                mMap.addMarker(new MarkerOptions().position(nuevapos).title("Tu destino AQUI"));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 10:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Configuration_Buttons();
                }
        }
    }

    private void Configuration_Buttons() {
        My_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("NavigationDrawer", "Click en My_location");
                locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // GoogleMap.MAP_TYPE_NORMAL,GoogleMap.MAP_TYPE_HYBRID y GoogleMap.MAP_TYPE_SATELLITE
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //Deshabilitando mapas interiores
        mMap.setIndoorEnabled(false);
        LatLng arequipa = new LatLng(-16.3988900, -71.5350000);
        mMap.addMarker(new MarkerOptions().position(arequipa).title("Tu estas AQUI"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(arequipa, 14));
        mMap.setPadding(0, 300, 0, 0);
        AddCircleRegion(arequipa);
    }

    private void AddCircleRegion(LatLng Pos) {
        Circle circle = mMap.addCircle(new CircleOptions()
                .center(Pos)
                .radius(1500)
                .strokeColor(Color.BLUE)
        );//.fillColor(Color.BLUE)
    }

    private void GoToMyLocationZoom(double lt, double lg, float zoom) {
        LatLng newPos = new LatLng(lt, lg);
        Marker_My_position = mMap.addMarker(new MarkerOptions()
                .position(newPos)
                .title("Tu estas AQUI")
                .snippet("Latitud: "+Double.toString(lt)+" Longitud: "+Double.toString(lg))
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newPos, 14));
    }

}
